<?php

use Illuminate\Database\Seeder;
use App\Newsletter;

class NewsletterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()   
    {
        Newsletter::create([
            'mail' => 'davidacosta309@gmail.com',
            'url_source' => 'http://localhost:8000/store-newsletter',
            'status' => '1'
        ]);
        Newsletter::create([
            'mail' => 'info@alarmalia.com',
            'url_source' => 'No Aplica',
            'status' => '1'
        ]);
    }
}
