<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Newsletter;
use App\Newsletter as Newsletterr;
use App\Exports\NewsletterExport;
use Maatwebsite\Excel\Facades\Excel;

class NewsLetterController extends Controller
{
    public function index()
    {
    	$newsletters = Newsletterr::all();
        return view('newsletters.index', compact('newsletters'));
    }

    public function store(Request $request)
    {
        if ( ! Newsletter::isSubscribed($request->email) ) 
        {
            Newsletter::subscribePending($request->email);

            DB::insert('insert into newsletters (mail, url_source, status) values (?, ?, ?)', [$request->email, $request->url(), 1]);

            return redirect('/')->with('success', 'Gracias por Susbcribirse');
        }
        return redirect('/')->with('failure', 'Lo siento, usted ya se encuentra subscrito.');
            
    }

    public function subscribe($mail)
    {

        Newsletter::subscribeOrUpdate($mail);
        DB::update('update newsletters set status = 1 where mail = :mail', ['mail' => $mail]);

        return redirect()->route('newsletter.index')->with('success', 'Usuario Suscrito Exitosamente.');
    }

    public function unsubscribe($mail)
    {
		Newsletter::unsubscribe($mail, 'subscribers');
        DB::update('update newsletters set status = 0 where mail = :mail', ['mail' => $mail]);

        return redirect()->route('newsletter.index')->with('success', 'Usuario Unsuscrito Exitosamente.');
    }

    public function destroy($id)
    {
        $newsletters = Newsletterr::find($id);

        Newsletter::delete($newsletters->mail);
        DB::table("newsletters")->where('id',$id)->delete();
        return redirect()->route('newsletter.index')
                        ->with('success','Usuario Eliminado con Éxito');
    }

    public function export() 
    {
        return Excel::download(new NewsletterExport, 'users_newsletter.xlsx');
    }


}
