<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Alarma;
use DB;
use Illuminate\Support\Facades\Input;
use Storage;
use File;
use Crypt;

class AlarmasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cualquiercosa = Alarma::all();

        return view('alarmas.index', compact('cualquiercosa'));

    }


    public function create(Request $request)
    {
        return view('alarmas.create');
    }
 

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre_alarma' => 'required',
            'estado' => 'required',
        ]);

        $alarma = new Alarma;
        $alarma->nombre_alarma = $request->nombre_alarma;
        $alarma->estado = $request->estado;
        $alarma->save();

        return redirect()->route('alarmas.prueba')->with('success', 'Alarma Creada Exitosamente.');
    }

 
    public function edit($id)
    {
        $alarma = Alarma::find($id);
        return view('alarmas.edit',compact('alarma'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre_alarma' => 'required',
            'estado' => 'required',
        ]);


        $alarma = Alarma::find($id);
        $alarma->nombre_alarma = $request->nombre_alarma;
        $alarma->estado = $request->estado;
        $alarma->update();

        return redirect()->route('alarmas.prueba')->with('success', 'Alarma Modificada Exitosamente.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::table("advantages")->where('id',$id)->delete();
        return redirect()->route('advantages.index', $request->company_id)
                        ->with('success','Ventaja/Desventaja Eliminada con Éxito');
    }
}