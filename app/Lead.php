<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Lead extends Model {

	use SoftDeletes;

    protected $table = 'lead';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = true;

	protected $fillable = [
		'*',
	];

}