@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Lista de Páginas Creadas
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
	                <center>
					<a class="btn btn-primary btn-block " href="{{ route('pages.create')}}">Crear Nueva Página</a>		
					</center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>Nombre Página</th>
							<th>Tipo Página</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($pages as $page)
						<tr>
							<td>{{ $page->page_name }}</td>
							<td>
								@if($page->page_type == '0')
									Plantilla 1
								@endif
								@if($page->page_type == '1')
									Plantilla 2
								@endif
							</td>
							<td><center>

							</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
