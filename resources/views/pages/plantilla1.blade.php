<!DOCTYPE html>
<html lang="es">

@include('layouts.head')

<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->

    <br>
    <br>

    <section>
        <div id="carouselExampleControls" class="carousel slide" >
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block " width="100%" height="100%" src="{{URL::asset($banners->banner_url)}}"
                alt="First slide">
              <div class="carousel-caption">
                                        <h3 class="h3-responsive home-title">{{$banners->title}}</h3>
                                            <a href="{{ url($banners->link_url) }}" target="_blank" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
              </div>
            </div>
          </div>
        </div>
    </section>


    <!-- START CLIENT-LOGO -->
    <section class="client-logo pt-1 pb-1" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title-heading text-center">Alarmas de seguridad para la casa</h1>
                </div>         
            </div>
        </div>
    </section>
    <!-- END CLIENT-LOGO -->
		

    <!-- START COUNTER -->
    <section class="section counter pt-1 pb-1">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-12">
                    <p class="title-desc text-center text-white-50 mt-4">Cuando tu seguridad y la de tu familia es lo primero, la instalación de una alarma para casa es una solución que te ayudará a estar más protegido, a evitar posibles robos en tu vivienda y a preservar tu patrimonio.
                    <br><br>
                    Las alarmas para casa se deben adaptar a las necesidades de cada cliente y, además de ofrecerle seguridad, deben poseer otros valores añadidos que respondan a las peticiones del cliente y también a las propias características de la casa.</p>                                     	
                </div>                          
            </div>          
        </div>
        <br><br><br>
        

        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/group.png') }}"  class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Casa con jardín</h1>
                        <p class="title-desc text-center text-white-50 mt-4">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/group.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Sótanos, garajes o piscinas</h1>
                        <p class="title-desc text-center text-white-50 mt-4">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/group.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Segunda vivienda (residencia esporádica) </h1>
                        <p class="title-desc text-center text-white-50 mt-4">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/group.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Zona de urbanización con seguridad privada</h1>
                        <p class="title-desc text-center text-white-50 mt-4">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/group.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Casas pequeñas</h1>
                        <p class="title-desc text-center text-white-50 mt-4">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/group.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Casas grandes (con gran valor económico) </h1>
                        <p class="title-desc text-center text-white-50 mt-4">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>
                 <div class="col-lg-3">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset('img/group.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">Casa sin suministro eléctrico</h1>
                        <p class="title-desc text-center text-white-50 mt-4">Es una buena práctica la instalación de detectores de inundación, que ayudan a descubrir posibles inundaciones y fugas de agua.</p>
                    </div>
                </div>

            </div>
            <center>
                        <div class="mt-5">
                            <a href="" class="btn btn-custom  btn-round">COMPRAR AHORA</a>
                        </div>
                    </center>
                    <br><br><br>
            <div class="row">
                <div class="col-lg-12">
                    <center>
                        <img src="{{URL::asset('img/appsmoviles.png') }}" class="img-fluid" alt="" >
                    </center>
                    <p class="title-desc text-center text-white-50 mt-4"> APPS MOVILES</p>
                    <p class="title-desc text-center text-white-50 mt-4">Existen aplicaciones móviles desde las que controlar lo que pasa en la casa, así como recibir imágenes de las diferentes cámaras del sistema de seguridad. Este tipo de soluciones te permiten activar y desactivar la alarma de forma remota u obtener imágenes de tu casa en cualquier momento, estés donde estés.</p>                                       
                </div>
            </div>
        </div>


    </section>
    <!-- END COUNTER -->
		




    <!-- START SERVICES -->
    <section class="client-logo" id="services">
        <div class="container">
            
            <div class="row mt-4">
                <p class="title-desc text-center text-white-50 mt-4">El técnico especializado que visite la casa deberá tener en cuenta todos estos aspectos para ofrecer la alarma que se adapte mejor en cada caso. Él será el encargado de recomendar el tipo de elementos necesarios y el número de los mismos. Por ejemplo, el número de detectores volumétricos será mayor si dispones de una casa con varias alturas y con un número alto de habitaciones.
                <br><br>
                Cada casa tiene diferentes necesidades de seguridad. Por eso, en Alarmalia te ayudamos a encontrar la mejor solución en base a tus requerimientos específicos y características del inmueble que deseas proteger. 
                <br><br>
                Compara ahora y consigue, además, nuestra guía de seguridad gratuita, con la que conocer detalladamente los aspectos más importantes que debes tener en cuenta antes de contratar una alarma para tu casa.</p>
            </div>
        </div>
    </section>
    <!-- END SERVICES -->

    
    <!-- START COUNTER -->
    <section class="section counter">
        <div class="container">
            <div class="col-lg-12" class="row mt-5" id="counter">
                    <h4 class="text-center">ENCUENTRA TU ALARMA</h4>
            <p class="title-desc text-center text-white-50 mt-4" >Ahorra dinero y tiempo con nuestro comparador de alarmas.<br> 
Descubre en 3 minutos la alarma que mejor se adapta a ti.</p>
                    <center>
                        <div class="mt-5">
                            <a href="" class="btn btn-custom  btn-round">COMPARAR AHORA</a>
                        </div>
                    </center>
                </div>
            
        </div>
    </section>    <!-- END COUNTER -->

@include('layouts.footer')

</body>

</html>