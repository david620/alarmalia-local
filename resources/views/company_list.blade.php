<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <title>Comparador de Alarmas | Tu alarma al mejor precio | Alarmalia</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Alarmalia es un comparador de alarmas para el hogar y negocio en el que puedes encontrar la mejor alarma al mejor precio. ¡Descubre tu mejor alarma!" />
    <meta name="keywords" content="alarma, tyco alarmas, prosegur alarmas, vigilante" />
    <meta content="RkPeople" name="author" />
    <!-- favicon -->
    <link rel="shortcut icon" href="{{URL::asset('img/favicon.png')}}">

    <!-- css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />

    <!--Slider-->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/owl.transitions.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}"/>

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Archivo&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129577938-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-129577938-1');
	</script> 
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M77GWGH');
	</script>
	<!-- End Google Tag Manager -->
    <style type="text/css">
    .carousel-caption {
    top: 60%;
    transform: translateY(-50%);
    bottom: initial;
    left: 60%;
    -webkit-transform-style: preserve-3d;
    -moz-transform-style: preserve-3d;
    transform-style: preserve-3d;
    }
      
    /*  bhoechie tab */
    div.bhoechie-tab-container{
      z-index: 10;
      background-color: #ffffff;
      padding: 0 !important;
      border-radius: 4px;
      -moz-border-radius: 4px;
      border:1px solid #ddd;
      margin-top: 20px;
      margin-left: 50px;
      -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
      box-shadow: 0 6px 12px rgba(0,0,0,.175);
      -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
      background-clip: padding-box;
      opacity: 0.97;
      filter: alpha(opacity=97);
    }
    div.bhoechie-tab-menu{
      padding-right: 0;
      padding-left: 0;
      padding-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group{
      margin-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group>a{
      margin-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group>a .glyphicon,
    div.bhoechie-tab-menu div.list-group>a .fa {
      color: #5A55A3;
    }
    div.bhoechie-tab-menu div.list-group>a:first-child{
      border-top-right-radius: 0;
      -moz-border-top-right-radius: 0;
    }
    div.bhoechie-tab-menu div.list-group>a:last-child{
      border-bottom-right-radius: 0;
      -moz-border-bottom-right-radius: 0;
    }
    div.bhoechie-tab-menu div.list-group>a.active,
    div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
    div.bhoechie-tab-menu div.list-group>a.active .fa{
      background-color: #5A55A3;
      background-image: #5A55A3;
      color: #ffffff;
    }
    div.bhoechie-tab-menu div.list-group>a.active:after{
      content: '';
      position: absolute;
      left: 100%;
      top: 50%;
      margin-top: -13px;
      border-left: 0;
      border-bottom: 13px solid transparent;
      border-top: 13px solid transparent;
      border-left: 10px solid #5A55A3;
    }

    div.bhoechie-tab-content{
      background-color: #ffffff;
      /* border: 1px solid #eeeeee; */
      padding-left: 20px;
      padding-top: 10px;
    }

    div.bhoechie-tab div.bhoechie-tab-content:not(.active){
      display: none;
    }
    </style>

    
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M77GWGH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->

   <!-- START HOME -->
    <br>
    <br>

    <section>
		<div id="carouselExampleControls" class="carousel slide" >
		  <div class="carousel-inner">
		    <div class="carousel-item active">
		      <img class="d-block " width="100%" height="100%" src="{{URL::asset($banners->banner_url)}}"
		        alt="First slide">
		      <div class="carousel-caption">
		                                <h3 class="h3-responsive home-title">{{$banners->title}}</h3>
		                                    <a href="{{ url($banners->link_url) }}" target="_blank" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
		      </div>
		    </div>
		  </div>
		</div>
    </section>
    <!-- END HOME -->



    <!-- START COUNTER -->
    <section class="section counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                	<br><br>
                    <h1 class="title-heading text-center">Compañias</h1>
                    <p class="title-desc text-center text-white-50 mt-4">Aquí ira un bello texto introductorio sobre las diferentes compañías de alarma y el beneficio de la comparativa que ofrece Alarmalia.</p>
                        
                    <br><br><br>

                </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group">
                @foreach($companies as $company)
                <a href="#" class="list-group-item text-center">
                  <br/><center><img class="d-block " width="100%" height="100%" src="{{URL::asset($company->link_image)}}"></center>
                </a>
                @endforeach
              </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- flight section -->
                @foreach($companies as $company)
                <div class="bhoechie-tab-content" style="margin-top: 0;">
                    <center>
                    <h1 class="title-heading text-center">{{$company->name}}</h1>
                    </center>
                    <p class="title-desc text-center text-white-50 mt-4">{{$company->description}}</p>
                    <br><br><br>
                    <h1 class="title-heading text-center">PROS</h1>

                        <ul id="services-list" style="list-style-type: none;">
                        @foreach( (\App\Advantage::where('company_id', '=',$company->id)->where('advatage_disadvantage', '=', 0)->get()) as $advantage)                        
                        <li>
                          <a class="image">
                            <img src="{{URL::asset('img/icon/check.png') }}" width="30px" />
                          </a>{{$advantage->content}}
                        </li>
                        <br>
                        @endforeach
                        </ul>


                    <br><br><br>
                    <h1 class="title-heading text-center">CONTRAS</h1>

                        <ul id="services-list" style="list-style-type: none;">
                        @foreach( (\App\Advantage::where('company_id', '=',$company->id)->where('advatage_disadvantage', '=', 1)->get()) as $advantage)                        
                        <li>
                          <a class="image">
                            <img src="{{URL::asset('img/icon/close.png') }}" width="30px" />
                          </a>{{$advantage->content}}
                        </li>
                        <br>
                        @endforeach
                        </ul>


                    <div class="mt-5" align="center">
                            <a href="{{ route('companies.profile', $company->id) }}" class="btn btn-custom btn-round">Más Información</a>
                    </div>


                </div>
                @endforeach






            </div>

            </div>
            </div>          
        <br><br><br>

    </section>
    <!-- END COUNTER -->
		
    <!-- START PRICING -->
    <section class="section bg-light" id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title-heading text-center">¿Eres un proveedor de alarmas?</h1>
                    
                    <center><div class="mt-5">
                            <a href="" class="btn btn-secondary btn-sm btn-round">ANUNCIARME</a>
                        </div></center>
                </div>
            </div>
        </div>
    </section>
    <!-- END PRICING -->

    
    <!-- START COUNTER -->
    <section class="section counter">
        <div class="container">
            <div class="col-lg-12" class="row mt-5" id="counter">
                    <h4 class="text-center">ENCUENTRA TU ALARMA</h4>
        	<p class="title-desc text-center text-white-50 mt-4" >Ahorra dinero y tiempo con nuestro comparador de alarmas.<br> 
Descubre en 3 minutos la alarma que mejor se adapta a ti.</p>
                    <center>
                        <div class="mt-5">
                            <a href="" class="btn btn-custom  btn-round">COMPARAR AHORA</a>
                        </div>
                    </center>
                </div>
            
        </div>
    </section>
    <!-- END COUNTER -->
 
@include('layouts.footer')

     <script type="text/javascript">
        $(document).ready(function() {
            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });

    </script>
</body>

</html>