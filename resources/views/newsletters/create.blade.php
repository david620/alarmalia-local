@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Crear Formulario Comparador
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')
  {!! Form::open(array('route' => 'forms.store','method'=>'POST')) !!}
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
                  <center>
                  <a class="btn btn-primary btn-block" href="{{ route('pages.index')}}">Regresar</a>    
                  </center>
                  <center>
                  <button id="cortoform" type="submit" class="btn btn-success btn-block hidden">Crear Página</button>    
                  </center>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-10 col-md-offset-1">
 

           <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Crear Página</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form">
                <div class="box-body">
                  <div class="form-group">
                    <label for="page_name">Nombre Página:</label>
  		            {!! Form::text('page_name', null, array('placeholder' => 'Nombre Página','class' => 'form-control', 'required' => 'required')) !!}
                  </div>
                  <div class="form-group">
                    <label for="page_title">Título Página:</label>
                  {!! Form::text('page_title', null, array('placeholder' => 'Título Página','class' => 'form-control', 'required' => 'required')) !!}
                  </div>
                  <div class="form-group">
                      <label for="page_type">Tipo Página:</label>
                        {!! Form::select('page_type',['0' => 'Plantilla 1', '1' => 'Plantilla 2',], '', array('class' => 'form-control', 'id' => 'tipo', 'placeholder' => 'Seleccionar', 'required' => 'required')) !!}
                  </div>   
                  <div class="form-group">
                    <label for="banner_url">Subir Banner:</label>
                          {!!Form::file('banner_url', array('class' => 'form-control'))!!}
                  </div>
                  <div class="form-group">
                    <label for="description_home">Contenido Primer Párrafo</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Contenido Primer Párrafo','class' => 'form-control', 'id' => 'summary-ckeditor', 'name' => 'description_home')) !!}
                  </div>

            <div class="col-md-12">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="description_home">Contenido Columna 1</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Guía gratuita de compras de alarmas ','class' => 'form-control', 'id' => 'summary-ckeditor2', 'name' => 'description_home')) !!}
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="description_home">Contenido Columna 2</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Guía gratuita de compras de alarmas ','class' => 'form-control', 'id' => 'summary-ckeditor3', 'name' => 'description_home')) !!}
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="description_home">Contenido Columna 3</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Guía gratuita de compras de alarmas ','class' => 'form-control', 'id' => 'summary-ckeditor4', 'name' => 'description_home')) !!}
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="description_home">Contenido Columna 4</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Guía gratuita de compras de alarmas ','class' => 'form-control', 'id' => 'summary-ckeditor5', 'name' => 'description_home')) !!}
                  </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="description_home">Contenido Columna 5</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Guía gratuita de compras de alarmas ','class' => 'form-control', 'id' => 'summary-ckeditor6', 'name' => 'description_home')) !!}
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="description_home">Contenido Columna 6</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Guía gratuita de compras de alarmas ','class' => 'form-control', 'id' => 'summary-ckeditor7', 'name' => 'description_home')) !!}
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="description_home">Contenido Columna 7</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Guía gratuita de compras de alarmas ','class' => 'form-control', 'id' => 'summary-ckeditor8', 'name' => 'description_home')) !!}
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="description_home">Contenido Columna 8</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Guía gratuita de compras de alarmas ','class' => 'form-control', 'id' => 'summary-ckeditor9', 'name' => 'description_home')) !!}
                  </div>
                </div>
            </div>

            <label>Botón Central</label>
            <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="description_home">Texto Botón</label>
                    {!! Form::text('page_name', null, array('placeholder' => 'Texto Botón','class' => 'form-control', 'required' => 'required')) !!}
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="description_home">Url Botón</label>
                    {!! Form::text('page_name', null, array('placeholder' => 'Url Botón','class' => 'form-control', 'required' => 'required')) !!}
                  </div>
                </div>
            </div>
                  <div class="form-group">
                    <label for="page_title">Título Segundo Párrafo:</label>
                  {!! Form::text('page_title', null, array('placeholder' => 'Título Segundo Párrafo','class' => 'form-control', 'required' => 'required')) !!}
                  </div>

                  <div class="form-group">
                    <label for="description_home">Contenido Segundo Párrafo</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Contenido Segundo Párrafo','class' => 'form-control', 'id' => 'summary-ckeditor10', 'name' => 'description_home')) !!}
                  </div>
                  <div class="form-group">
                    <label for="description_home">Contenido Tercer Párrafo</label>
                  {!! Form::textarea('description_home', '', array('placeholder' => 'Contenido Tercer Párrafo','class' => 'form-control', 'id' => 'summary-ckeditor11', 'name' => 'description_home')) !!}
                  </div>

                      <div class="form-group">
                        <label for="find_alarm">Incluir Sección: ENCUENTRA TU ALARMA</label>
                                {!! Form::checkbox('find_alarm', '1'); !!}
                      </div>
                      <div class="form-group">
                        <label for="alarm_provider">Incluir Sección: ¿Eres un proveedor de alarmas?</label>
                                {!! Form::checkbox('alarm_provider', '1'); !!}
                      </div>
            </div>
              </form>
        </div>
    </div>
  {!! Form::close() !!}
@endsection

@section('js')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'summary-ckeditor' );
        CKEDITOR.replace( 'summary-ckeditor2' );
        CKEDITOR.replace( 'summary-ckeditor3' );
        CKEDITOR.replace( 'summary-ckeditor4' );
        CKEDITOR.replace( 'summary-ckeditor5' );
        CKEDITOR.replace( 'summary-ckeditor6' );
        CKEDITOR.replace( 'summary-ckeditor7' );
        CKEDITOR.replace( 'summary-ckeditor8' );
        CKEDITOR.replace( 'summary-ckeditor9' );
        CKEDITOR.replace( 'summary-ckeditor10' );
        CKEDITOR.replace( 'summary-ckeditor11' );
    </script>
	<script type="text/javascript">
		$('#tipo').on('change', function() {
            if (this.value == '0') {
                $('#cortos').show().removeClass('hidden');
                $('#cortoform').show().removeClass('hidden');
                $('#largosform').show().addClass('hidden');
                $('#largos').show().addClass('hidden');

            } 
            if (this.value == '1'){
                $('#cortos').show().addClass('hidden');
                $('#largos').show().removeClass('hidden');
                $('#largosform').show().removeClass('hidden');
                $('#cortoform').show().addClass('hidden');
            }
            if (this.value == ''){
                $('#cortos').show().addClass('hidden');
                $('#largos').show().addClass('hidden');
                $('#cortoform').show().addClass('hidden');
                $('#largosform').show().addClass('hidden');

            }
		});
	</script>
@stop