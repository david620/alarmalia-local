<!DOCTYPE html>
<html lang="es">

@include('layouts.head')

<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->

    <br>
    <br>

    <section>
        <div id="carouselExampleControls" class="carousel slide" >
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block " width="100%" height="100%" src="{{URL::asset($banners->banner_url)}}"
                alt="First slide">
              <div class="carousel-caption">
                                        <h3 class="h3-responsive home-title">{{$banners->title}}</h3>
                                            <a href="{{ url($banners->link_url) }}" target="_blank" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
              </div>
            </div>
          </div>
        </div>
    </section>


<!-- START CONTACT -->
    <section class="section" id="contact">
        <div class="container">
            <div class="row justify-content-center mt-5">
                <div class="col-lg-10">
                    <div class="col-lg-12">
                    <h4 class="text-center">Anúnciate</h4>
            <p class="title-desc text-center text-white-50 mt-4" >¿Eres una empresa de alarmas o distribuidor de alarmas y quieres formar parte de nuestro comparador? Rellena el siguiente formulario e infórmate de cómo puedes hacerlo.</p>
                </div>
                    <div class="custom-form mt-3">
                        <div id="message"></div>
                            {!! Form::open(array('route' => 'advertise.store','method'=>'POST')) !!}
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                        {!! Form::text('name', null, array('placeholder' => 'Nombre: ','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                        {!! Form::text('lastname', null, array('placeholder' => 'Apellido: ','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                        {!! Form::text('company_name', null, array('placeholder' => 'Empresa: ','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                        {!! Form::email('company_email', null, array('placeholder' => 'Email: ','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                        {!! Form::text('phone', null, array('placeholder' => 'Teléfono: ','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                        {!! Form::textarea('message', null, array('placeholder' => 'Mensaje: ','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                    <div class="checker" id="uniform-customer_privacy">
                                      <input type="checkbox" value="0" required  name="terms" autocomplete="off"> <a class="f-18" style="color: #666666;" href="{{ route('companies.policies') }}"><b> He leído y acepto la política de privacidad</b></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <input id="submit" name="send" class="submitBnt btn btn-secondary btn-round" value="ENVIAR" type="submit">
                                    <div id="simple-msg"></div>
                                </div>
                            </div>
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END CONTACT -->

     <!-- START COUNTER -->
    <section class="section counter">
        <div class="container">
            <div class="col-lg-12" class="row mt-5" id="counter">
                    <h4 class="text-center">ENCUENTRA TU ALARMA</h4>
            <p class="title-desc text-center text-white-50 mt-4" >Ahorra dinero y tiempo con nuestro comparador de alarmas.<br> 
Descubre en 3 minutos la alarma que mejor se adapta a ti.</p>
                    <center>
                        <div class="mt-5">
                            <a href="{{ route('companies.comparator')}}" class="btn btn-custom  btn-round">COMPARAR AHORA</a>
                        </div>
                    </center>
                </div>
            
        </div>
    </section>
    <!-- END COUNTER -->

 
@include('layouts.footer')

</body>

</html>