    <section class="section counter">
        <div class="container">
            <div class="col-lg-12" class="row mt-5" id="counter">
                    <h4 class="text-center">ENCUENTRA TU ALARMA</h4>
                    <div class="title-desc text-center text-white-50 mt-4" >
                        <p>Ahorra dinero y tiempo con nuestro comparador de alarmas.<br> 
                        Descubre en 3 minutos la alarma que mejor se adapta a ti.
                        </p>
                    </div>
                    <center>
                        <div class="mt-5">
                            <a href="{{ route('companies.comparator')}}" class="btn btn-custom  btn-round">COMPARAR AHORA</a>
                        </div>
                    </center>
                </div>
            
        </div>
    </section>