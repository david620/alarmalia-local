<!DOCTYPE html>
<html lang="es">

@include('layouts.head')

<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->


    <br>
    <br>

    <section>
        <div id="carouselExampleControls" >
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block " width="100%" height="100%" src="{{URL::asset($banners->banner_url)}}"
                alt="First slide">
              <div class="carousel-caption">
                                        <h3 class="h3-responsive home-title">{{$banners->title}}</h3>
                                            <a href="{{ url($banners->link_url) }}" target="_blank" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
              </div>
            </div>
          </div>
        </div>
    </section>


 



     <!-- START COUNTER -->
    <section class="section counter mt-6">
        <div class="container">
            <h1 class="title-heading text-center">Sistemas de alarmas para casa y negocio</h1>
            <div class="row">
                @foreach($posts as $post)
                <div class="col-lg-6">
                    <div class="servicios-box bg-white btn-rounde mt-4">
                        <div class="service-icon text-center">
                            <img src="{{URL::asset($post->url_img) }}"  class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headin text-center">{{$post->title}}</h1>
                        <p class="title-desc text-justify text-white-50 mt-4">{{trim(substr($post->content, 0, 200))}}...</p>
                        <br>
                        <a href="{{route('post.blog', $post->id)}}" class="btn btn-secondary  btn-round">Leer nota completa</a>
                    </div>
                </div>                
                @endforeach
            </div>            
        </div>
 
    </section>
    <!-- END COUNTER -->
    <section class="section counter">
        <div class="container">
            <div class="col-lg-12" class="row mt-5" id="counter">
                    <h4 class="text-center">ENCUENTRA TU ALARMA</h4>
                    <p class="title-desc text-center text-white-50 mt-4">
                        Ahorra dinero y tiempo con nuestro comparador de alarmas.<br> 
                        Descubre en 3 minutos la alarma que mejor se adapta a ti.
                    </p>
                    <center>
                        <div class="mt-5">
                            <a href="" class="btn btn-custom  btn-round">COMPARAR AHORA</a>
                        </div>
                    </center>
                </div>
            
        </div>
    </section>
 
@include('layouts.footer')

</body>

</html>