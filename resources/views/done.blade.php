<!DOCTYPE html>
<html lang="es">
@include('layouts.head')
<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->

    <br>
    <br>

    <section>
        <div id="carouselExampleControls" class="carousel slide" >
          <div class="carousel-inner">
            <div class="item active">
              <img class="d-block " width="100%" height="100%" src="{{URL::asset($banners->banner_url)}}"
                alt="First slide">
              <div class="carousel-caption">
                                        <h3 class="h3-responsive home-title">{{$banners->title}}</h3>
                                            <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
              </div>
            </div>
          </div>
        </div>
    </section>
    <br>
    <br>
    <br>
    <br>


 
<!-- START CONTACT -->
    <section class="section" id="contact">
        <div class="container">
            <div class="row justify-content-center mt-5">
                <div class="col-lg-12">
                    <div class="col-lg-12">
                        <center>
                        <img src="{{URL::asset('img/unnamed.png') }}" alt="">
                        </center>
                        <br><br>
                        <center>
                        <div class="col-lg-6 center">
                    <h4 class="title-heading">¡BIEN HECHO!<br>
                En breve nos pondremos en contacto con tu empresa para explicarte cómo formar parte de nuestro comparador.</h4>
            </div>
            </center>
            
               
            </div>
        </div>
    </section>
    <!-- END CONTACT -->

     <!-- START COUNTER -->
    <section class="section counter">
        <div class="container">
            <div class="col-lg-12" class="row mt-5" id="counter">
                    <h4 class="text-center">ENCUENTRA TU ALARMA</h4>
            <p class="title-desc text-center text-white-50 mt-4" >Ahorra dinero y tiempo con nuestro comparador de alarmas.<br> 
Descubre en 3 minutos la alarma que mejor se adapta a ti.</p>
                    <center>
                        <div class="mt-5">
                            <a href="" class="btn btn-custom  btn-round">COMPARAR AHORA</a>
                        </div>
                    </center>
                </div>
            
        </div>
    </section>
    <!-- END COUNTER -->

@include('layouts.footer')
</body>

</html>