<!DOCTYPE html>
<html lang="es">
@include('layouts.head')

<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->


    <br>
    <br>

    <section>
        <div id="carouselExampleControls" class="carousel slide" >
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block " width="100%" height="100%" src="{{URL::asset($banners->banner_url)}}" alt="First slide">
              <div class="carousel-caption">
                                        <h3 class="h3-responsive home-title">{{$banners->title}}</h3>
                                            <a href="{{ url($banners->link_url) }}" target="_blank" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
              </div>
            </div>
          </div>
        </div>
    </section>


<!-- START CONTACT -->
    <section class="section" id="contact">
        <div class="container">
            <div class="row justify-content-center mt-5">
                <div class="col-lg-10">
                    <div class="col-lg-12">
                    <h4 class="text-center">PREGUNTAS FRECUENTES</h4>
            
            <br><br>
    @foreach($categories as $categorie)
            <div class="accordion" id="accordionExample">
                <br>                
                <center><img class="d-block " width="10%" height="10%" src="{{URL::asset($categorie->category_icon)}}" alt="First slide"> <h3 class="title-headin text-center">{{ $categorie->category_name }}</h3></center>
                <br>
                @foreach( (\App\Faq::where('faq_category_id', '=',$categorie->id)->get()) as $faq)
                  <div class="card">
                    <div class="card-header" id="headingOne">
                      
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $faq->id }}" aria-expanded="true" aria-controls="collapse{{ $faq->id }}">
                          <h1 class="title-headin text-center">{{ $faq->question }}</h1>
                        </button>
                      
                    </div>

                    <div id="collapse{{ $faq->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body">
                        <p align="justify">{{ $faq->content }} </p>
                      </div>
                    </div>
                  </div>
                @endforeach
            </div>
    @endforeach
            </div>
        </div>
    </section>
    <!-- END CONTACT -->

     <!-- START COUNTER -->
    <section class="section counter">
        <div class="container">
            <div class="col-lg-12" class="row mt-5" id="counter">
                    <h4 class="text-center">ENCUENTRA TU ALARMA</h4>
            <p class="title-desc text-center text-white-50 mt-4" >Ahorra dinero y tiempo con nuestro comparador de alarmas.<br> 
Descubre en 3 minutos la alarma que mejor se adapta a ti.</p>
                    <center>
                        <div class="mt-5">
                            <a href="" class="btn btn-custom  btn-round">COMPARAR AHORA</a>
                        </div>
                    </center>
                </div>
            
        </div>
    </section>
    <!-- END COUNTER -->

 
@include('layouts.footer')
</body>

</html>