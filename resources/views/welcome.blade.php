<!DOCTYPE html>
<html lang="es">
@include('layouts.head')
<!-- Vendor CSS -->
        
        <link rel="stylesheet" href="{{ asset('css/carousel/font-awesome.min.css') }}">                
        <link rel="stylesheet" href="{{ asset('css/carousel/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/carousel/owl.theme.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/carousel/theme-elementos.css') }}">
 
<body>
  <!--Navbar Start-->
@include('layouts.header')
  <!-- Navbar End -->

   <!-- START HOME -->
  <section class="bg-home-1" id="home">
        <div class="home-bg-overlay"></div>
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container">
                    <div class="row vertical-content">
                        <div class="col-lg-6">
                            <div class="home-img mt-4">
                                <img src="{{URL::asset('img/ALARMAS_homeDT.png') }}" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="home-content">
                                <h3 class="home-title">Tu comparador de alarmas de casa y negocio</h3>
                                
                                <div class="mt-5">
                                    <center>
                                    <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">COMPARAR ALARMAS </a>
                                    </center>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    <!-- START CLIENT-LOGO -->
    <section class="client-logo pt-3 pb-3" >

        <div class="container">
            <div class="row">

                <div class="col-lg-1">
                </div>
                <div class="col-lg-2 " >
                    <center>
                    <div class="client-img">
                            <p align="center">
                            <img   src="{{URL::asset('img/alarmalia/click-copy.png') }}" alt="logo-img">
                            </p>
                    </div>
                    </center>                        
                    <p class="title-desc text-center text-white-50 mt-4" >Todas las ofertas<br> en un click</p>
                </div>
                <div class="col-lg-2">
                </div>
                <div class="col-lg-2">
                    <center>
                    <div class="client-img">
                        <img  src="{{URL::asset('img/alarmalia/customer-copy.png') }}" alt="logo-img" class="mx-auto img-fluid d-block">
                    </div>
                    </center>
                    <p class="title-desc text-center text-white-50 mt-4">Recibe un estudio personalizado</p>
                </div>
                <div class="col-lg-2">
                </div>
                <div class="col-lg-2 ">
                    <center>
                    <div class="client-img">
                        <img src="{{URL::asset('img/alarmalia/book-copys.png') }}" alt="logo-img" class="mx-auto img-fluid d-block">
                    </div>
                    </center>
                    <p class="title-desc text-center text-white-50 mt-4">Guía gratis con selección de alarmas</p>
                </div>
                <div class="col-lg-1">
                </div>                
            </div>
        </div>
    </section>
    <!-- END CLIENT-LOGO -->


		



    <!-- START COUNTER -->
    <section class="section counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    	<br><br>
                        <h1 class="title-heading text-center">Guía gratuita de compras de alarmas</h1>
                        <br>
                        <div class="title-desc text-center text-white-50 mt-4">
                        <p >{!!$homepages->description_home!!}</p>
                        </div>
                        <br>
                        <div class="mt-5" align="center">
                                <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">QUIERO MI GUIA</a>
                        </div>
                        

                        <div class="col-lg-12">
                        <br><br>
                        <h2 class="title-desca text-center mt-4">Empresas con las que trabajamos</h2>
                        <br><br>
                        <center>
                        <div class="col-lg-8 owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="{{URL::asset('imagenes/companies/tyco.jpg') }}">
                                </div>
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="{{URL::asset('imagenes/companies/visegur.png') }}">
                                </div>
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="{{URL::asset('imagenes/companies/tyco.jpg') }}">
                                </div>
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="{{URL::asset('imagenes/companies/visegur.png') }}">
                                </div>
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="{{URL::asset('imagenes/companies/tyco.jpg') }}">
                                </div>
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="{{URL::asset('imagenes/companies/visegur.png') }}">
                                </div>
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="{{URL::asset('imagenes/companies/tyco.jpg') }}">
                                </div>
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="{{URL::asset('imagenes/companies/visegur.png') }}">
                                </div>
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="{{URL::asset('imagenes/companies/tyco.jpg') }}">
                                </div>
                                <div>
                                    <img alt="" class="img-responsive img-rounded" src="{{URL::asset('imagenes/companies/visegur.png') }}">
                                </div>
                            </div></center>
                        </div>
                </div>
        <div class="container">
            <h2 class="title-desca text-center mt-4">¿Qué alarma buscas?</h2>
            <br><br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="service-box pt-2 pb-3 text-center"  >
                        <a class=" text-center" style="color: #ffffff" href="{{ route('companies.homealarms')}}"><p> <img src="{{URL::asset('img/icon/home.png') }}" alt="" >Alarmas para Casa<br> <u>ver</u></p> </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="servicess-box pt-2 pb-3 text-center">
                        <a class=" text-center" style="color: #ffffff" href="{{ route('companies.homealarms')}}"><p> <img src="{{URL::asset('img/icon/shop.png') }}" alt="" >Alarmas para Negocio<br> <u>ver</u></p> </a>
                    </div>              
                </div>                
            </div>
        </div>

			
                
            


            </div>          

        </div>
        <br><br><br>
        <div class="col-lg-12">
        	<h1 class="title-heading text-center">¿Como seleccionar una alarma?</h1>
            <br>
            <div class="title-desc text-center text-white-50 mt-4">
                        <p >{{$homepages->select_alarm}}</p>
                        </div>	
                <br>
                <div class="col-lg-12 selector-form">
                 	<div class="title-desc text-center text-white-50">

                 		<p> ¿Qué pasa ante un salto de alarma?  <br> ¿tengo que preocuparme de algo?</p>
                 		<button><h4> 1</h4></button>
                 	</div>
                 </div>

                 <div class="col-lg-12 selector-for">
                 	<div class="title-desc text-center text-white-50">
                 		<p> ¿Todos los sistemas de alarma me protegen además de ante <br>una posible intrusión, de intentos de sabotaje o inhibición?</p>
                 		<button><h4> 2</h4></button>
                 	</div>
                 </div>

                 <div class="col-lg-12 selector-fo">
                 	<div class="title-desc text-center text-white-50">
                 		<p> ¿Qué servicios adicionales tiene cada sistema de alarma <br> para sentirme completamente seguro?</p>
                 		<button><h4> 3</h4></button>
                 	</div>
                 </div>








					
					<div class="mt-5" align="center">
                            <a href="{{ route('companies.comparator')}}" class="btn btn-custom btn-round">COMPARAR AHORA</a>
                    </div>

                    <br><br><br>

                    <center><img src="{{URL::asset('img/alarmalia/candado.png') }}" width="40%" height="40%" alt="logo-img"></center>

                    <br><br><br>
					<h4 class="text-center pb-3">¿Por qué utilizar ALARMALIA?</h4>
                    <div class="title-desc text-center text-white-50 mt-4">
                        <p >{{$homepages->description_middle}}</p>
                        </div>          
        	

                </div>
    </section>
    <!-- END COUNTER -->
		




    <!-- START SERVICES -->
    <section style="background-color: #EFF5F8;" class="section"  id="services">
        <div class="container">
            <center>
            <div class="row mt-5 pt-3">
                <div class="col-lg-4">
                    <div class="services-box bg-white p-5 btn-rounde mt-4" style="width: 22rem; height: 25rem;">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/conocimientomercado.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headingg text-center">CONOCIMIENTO DEL MERCADO</h1>
                        <div class="title-desc text-center text-white-50 mt-4">
                        <p >{{$homepages->card_one}}</p>
                        </div>          
                        
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="services-box bg-white p-5 btn-rounde mt-4" style="width: 22rem; height: 25rem;">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/ahorro.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headingg text-center">AHORRO</h1>
                        <div class="title-desc text-center text-white-50 mt-4">
                        <p >{{$homepages->card_two}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="services-box bg-white p-5 btn-rounde mt-4" style="width: 22rem; height: 25rem;">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/serviciogratuito.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headingg text-center">SERVICIO GRATUITO</h1>
                        <div class="title-desc text-center text-white-50 mt-4">
                        <p>{{$homepages->card_three}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-lg-4">
                    <div class="services-box bg-white p-5 btn-rounde mt-4" style="width: 22rem; height: 25rem;">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/rapidez.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headingg text-center">RAPIDEZ</h1>
                        <div class="title-desc text-center text-white-50 mt-4">
                        <p>{{$homepages->card_four}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="services-box bg-white p-5 btn-rounde mt-4" style="width: 22rem; height: 25rem;">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/resultadospersonalizados.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headingg text-center">RESULTADOS PERSONALIZADOS</h1>
                        <div class="title-desc text-center text-white-50 mt-4">
                        <p>{{$homepages->card_five}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="services-box bg-white p-5 btn-rounde mt-4" style="width: 22rem; height: 25rem;">
                        <div class="services-icon text-center">
                            <img src="{{URL::asset('img/alarmalia/somosindependientes.png') }}" class="img-fluid" alt="">
                        </div><br>
                        <h1 class="title-headingg text-center">SOMOS INDEPENDIENTES</h1>
                        <div class="title-desc text-center text-white-50 mt-4">
                        <p>{{$homepages->card_six}}</p>
                        </div>
                    </div>
                </div>
            </div>
            </center>
        </div>
    </section>
    <!-- END SERVICES -->

    
    <!-- START COUNTER -->
@include('layouts.find_alarm')
    <!-- END COUNTER -->

    <!-- START PRICING -->
@include('layouts.alarm_provider')
    <!-- END PRICING -->
 
@include('layouts.footer')
 
        <script src="{{ asset('js/carousel/jquery.min.js') }}"></script>
        <script src="{{ asset('js/carousel/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/carousel/theme.js') }}"></script>
        <script src="{{ asset('js/carousel/theme.init.js') }}"></script>

<script type="text/javascript">

$('#carouselExample').on('slide.bs.carousel', function (e) {

    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;
    
    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});

</script>

</body>

</html>