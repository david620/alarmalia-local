<!DOCTYPE html>
<html lang="es">
@include('layouts.head')
<body>

  <!--Navbar Start-->
@include('layouts.header')
    <!-- Navbar End -->

 
    <br>

    <section>
    </section>
 <!-- START CLIENT-LOGO -->
    <section class="cliente-logo pt-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <h3 class="title-headin">¡ Falta poco para saber sobre tu alarma ideal! </h3>
                    
                    <br>
            <div class="progress">
                <div class="progress-bar" style="width:1%; background:#614FA2;">
                    <span ></span>
                    <div class="progress-value"><span><img src="{{URL::asset('img/alarmalia/claro.png') }}" alt="" height="100%" width="100%"></span></div>
                </div>
            </div>
            
            <h3 class="title-headin"> </h3>
                </div>
                            
            </div>
        </div>
    </section>
    <!-- END CLIENT-LOGO -->
 
<!-- START CONTACT -->
    <section class="section" id="contact">
        <div class="container">
            <div class="row justify-content-center mt-5">
                <div class="col-lg-18">
                    {!! Form::open(array('route' => 'comparator.store','method'=>'POST')) !!}
                    {{ csrf_field() }}
                    <div class="col-lg-12">
                        <h4 class="title-heading">{{$question->title}}</h4>            
                        <br><br>
                        <div class="quiz" id="quiz"  >
                            @foreach($answers as $answer)
                               <label class="element-animation2 btn btn-lg btn-primary btn-block"> 
                               <input style="color: #212529;" type="radio" name="q_answer" value="1"> <a style="color: #212529;" class="sm2_link" href="{{route('companies.comparator_camino', $answer->next_question)}}">{{$answer->content}}</a>  
                               </label>
                            @endforeach
                        </div>

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <!-- END CONTACT -->

@include('layouts.footer')

    <script type="text/javascript">
        $('.sm2_link').on('click', function () {
          this.parentNode.click()
        });        
    </script>


</body>

</html>